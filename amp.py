import math
import numpy as np
import pyaudio
from slowq import SlowQ, midi_freq

def main():
	Amp().run()

class Amp:
	def __init__(self):
		self.pyaud = pyaudio.PyAudio()
		self.sample_rate = int(self.pyaud.get_device_info_by_index(0)['defaultSampleRate'])
		self.chunk_size = int(self.sample_rate * 0.01)
		
		# Low E <= range <= High E, fret 24
		self.midi_min = 40
		self.midi_max = self.midi_min + 12 * 4 + 1
		
		minFreq = midi_freq(self.midi_min)
		maxFreq = midi_freq(self.midi_max)
		self.sq = SlowQ(minFreq, maxFreq, 12, self.sample_rate)
		self.cq = self.sq.make_out()
		
		self.wave = np.zeros(self.sq.get_input_size())
		self.dtype = np.int16
		di = np.iinfo(self.dtype)
		self.raw_wave_amplitude = max(abs(di.min), di.max)
	
	def run(self):
		stream = self.pyaud.open(
			format = pyaudio.paInt16,
			channels = 1, rate = self.sample_rate,
			frames_per_buffer = self.chunk_size,
			input = True, output = True,
			stream_callback = self._stream_callback,
		)
		
		app = _build_gui(self)
		
		try:
			app.run()
		finally:
			stream.stop_stream()
			stream.close()
			self.pyaud.terminate()
	
	def _stream_callback(self, in_data, frame_count, time_info, status):
		self._add_chunk(in_data)
		return (in_data, pyaudio.paContinue)
	
	def _add_chunk(self, chunk):
		wave = np.fromstring(chunk, dtype = self.dtype).astype(np.float32)
		wave /= self.raw_wave_amplitude
		w = self.wave
		l = len(wave)
		w[:-l] = w[l:]
		w[-l:] = wave

def _build_gui(amp):
	from pyqtgraph.Qt import QtGui, QtCore
	import pyqtgraph as pg
	
	app = QtGui.QApplication([])
	win = pg.GraphicsWindow(title = "Amp")
	win.resize(800, 600)
	pg.setConfigOptions(antialias = True)
	
	x = np.arange(len(amp.wave))
	
	p1 = win.addPlot(title = "Raw Data", row = 0, col = 0)
	p1.enableAutoRange('xy', False)
	p1.setXRange(0, len(amp.wave))
	p1.setYRange(-1, 1)
	p1c = p1.plot(pen = 'y', x = x)
	
	midi_del = amp.midi_max - amp.midi_min
	
	b1 = win.addPlot(title = "cQ Transform", row = 1, col = 0)
	b1.enableAutoRange('xy', False)
	b1.setXRange(0, midi_del)
	b1.setYRange(0, 0.1)
	b1c = pg.BarGraphItem(x = np.arange(midi_del), height = 0, width = 0.3, brush = 'r')
	b1.addItem(b1c)
	
	cq = amp.cq
	wave = amp.wave
	sq = amp.sq
	
	def update():
		p1c.setData(wave)
		sq.run(wave, cq)
		b1c.setOpts(height = cq)
	timer = QtCore.QTimer()
	timer.timeout.connect(update)
	timer.start(50)
	
	return QtAppWrapper(app, [win, timer])

class QtAppWrapper:
	# Need to keep a reference to the deps,
	# otherwise they get destroyed.
	def __init__(self, app, deps):
		self.app = app
		self._deps = deps
	
	def run(self):
		self.app.exec_()

if __name__ == '__main__':
	main()
