import numpy as np
from math import ceil, log

TAU = 2 * np.pi

def main():
	from matplotlib import pyplot as plt
	
	minFreq = midi_freq(40)
	maxFreq = midi_freq(88)
	bins = 12
	samplingRate = 44100
	
	sq = SlowQ(minFreq, maxFreq, bins, samplingRate)
	out = sq.make_out()
	
	for nf in [30000]:
		x = np.sin(np.arange(nf) / 10)
		sq.run(x, out)
		plt.plot(out)
	plt.show()

def hamming(N):
	alpha = 0.5
	return alpha - (1 - alpha) * np.cos(TAU * np.arange(N) / N)

def midi_freq(num):
	return 440 * 2 ** ((num - 69) / 12)

# http://doc.ml.tu-berlin.de/bbci/material/publications/Bla_constQ.pdf
class SlowQ:
	def __init__(self, minFreq, maxFreq, bins, samplingRate):
		self.K = ceil(bins * log(maxFreq/minFreq, 2))
		self.Q = 1/(2**(1/bins) - 1)
		self.r = self.Q * samplingRate / minFreq
		self.bins = bins
		
		# Precompute weights
		self.weights = []
		
		for k in range(self.K):
			N = round(self.r / 2**(k/self.bins))
			window = hamming(N)
			transform = np.exp(-TAU * 1j * self.Q / N * np.arange(N))
			self.weights.append(window * transform / N)
	
	def get_input_size(self):
		return len(self.weights[0])
	
	def make_out(self):
		return np.zeros(self.K)
	
	def run(self, x, out, k0 = None):
		k = k0 or self.K
		while k > 0:
			k -= 1
			w = self.weights[k]
			N = len(w)
			if N > len(x):
				return k + 1
			out[k] = np.abs(x[-N:] @ w)
		return None
